#!/bin/bash
libraryDir=$1
arduinoAfter=${2:-$HOME/.vim/after/syntax/arduino/}

usage () {
	tabs 2
	cat <<_USAGE
usage: ${0##*/} directory [after_directory]
	directory					Arduino library directory containing 'keywords.txt' files
	after_directory		VIM after directoy, default to: 
										${arduinoAfter}
_USAGE
}

if [ -z "$libraryDir" ]; then
	echo "${0##*/}: Missing library directory" >&2
	usage
	exit 1
fi

if [ ! -d "$libraryDir" ]; then
	echo "${0##*/}: Unable to access library directory" >&2
	exit 2
fi

mkdir -p ${arduinoAfter} 2>/dev/null
statFS=($(stat --format '%U %G %a' "${arduinoAfter}" 2> /dev/null ))

if [ -n "${statFS[*]}" ] ; then
	if [ ! "${statFS[0]}" = "${USERNAME}" ]; then
		if [[ ! groups == *${statFS[1]}* ]]; then
			if [ ! ${statFS[2]} == *7 ]; then
				echo "${0##*/}: permission denied to '${arduinoAfter}'" >&2
				exit 3
			fi
		fi	
	fi
else
	echo "${0##*/}: can't acces directory '${arduinoAfter}'" >&2
	exit 4
fi

sedExpr="/^[[:space:]]*#.*/d;"

#arduinoConstant
sedExpr+='/^[a-zA-Z0-9_]\+[[:space:]]\+LITERAL1/ { s,^[[:space:]]*\([^[:space:]]\+\)[[:space:]]*.*,syn keyword arduinoConstant \1,; p};'
#arduinoMethod
#arduinoType
sedExpr+='/^[a-zA-Z0-9_]\+[[:space:]]\+KEYWORD3/ { s,^\([^[:space:]]\+\)[[:space:]].*,syn keyword arduinoType \1,; p};'

#arduinoStdFunc
#arduinoFunc
sedExpr+='/^[a-zA-Z0-9_]\+[[:space:]]\+KEYWORD2/ { s,^\([^[:space:]]\+\)[[:space:]].*,syn keyword arduinoFunc \1,; p};'

#arduinoModule
sedExpr+='/^[a-zA-Z0-9_]\+[[:space:]]\+KEYWORD1/ { s,^\([^[:space:]]\+\)[[:space:]].*,syn keyword arduinoModule \1,; p};'


for fKeywords in $(find ${libraryDir} -iname 'keywords.txt'); do
	moduleName=${fKeywords#${libraryDir}}
	moduleName=${moduleName%/keywords.txt}
	moduleName=${moduleName//\//_}
	sed -e "$sedExpr" $fKeywords > ${arduinoAfter}/ARD2VIM_${moduleName}.vim
done

# vim: set tabstop=2 shiftwidth=2 cino=1 autoindent smartindent:
