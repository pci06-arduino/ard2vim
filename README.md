Simple bash script to convert ```keywords.txt``` files to VIM syntax file.
The generated files need ```arduino.vim```, this syntax file can be found in the ubuntu package named ```vim-runtime```

